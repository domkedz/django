import time
import socket
import telnetlib

class Connection(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.my_socket.settimeout(1)
        self.my_socket.connect((ip, self.port))
        self.my_socket.recv(512)


    def readOne(self, number):
        pass

    def readAll(self):
        pass

    def setOne(self, number, value):
        pass

    def setAll(self, value):
        pass

    def clearAll(self):
        pass

    def closingSocket(self):
        self.my_socket.close()

class RFSzte196(Connection): #port 23

    def readOne(self, numberOfSwitch):
        if numberOfSwitch > 12 or numberOfSwitch < 0:
            return "Value " + str(numberOfSwitch) + " isn't correct!"
        else:
            self.my_socket.send("GetSSW" + str(numberOfSwitch) + "\r\n")
            data = "SW " + str(numberOfSwitch) + ":  " + self.my_socket.recv(512)
            return data.replace("\r", "")

    def readAll(self):
        result = ""
        for i in range(1, 13):
            self.my_socket.send("GetSSW" + str(i) + "\r\n")
            result += "SW " + str(i) + ":  " + self.my_socket.recv(512)
        return result.replace("\r", "")


    def setOne(self, numberOfSwitch, numberOfPort):
        if numberOfSwitch > 12 or numberOfSwitch < 0:
            return "Value " + str(numberOfSwitch) + " isn't correct!"
        elif numberOfPort > 6 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.my_socket.send('C' + str(numberOfSwitch) + '=' + str(numberOfPort) + '\r\n')
            result = self.my_socket.recv(512)
            return result

    def setAll(self, numberOfPort):
        if numberOfPort > 6 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            result = ""
            for i in range(1, 13):
                self.my_socket.send("c" + str(i) + "=" + str(numberOfPort) + "\r\n")
                result += self.my_socket.recv(512)
            return result.replace("\r", "")

    def clearAll(self):
        return self.setAll(0)

class RFSztm15(Connection): #port 23

    def readOne(self, numberOfSwitch):
        if numberOfSwitch > 6 or numberOfSwitch < 0:
            return "Value " + str(numberOfSwitch) + " isn't correct!"
        else:
            self.my_socket.send("SP6T:" + str(numberOfSwitch) + ":STATE?\n")
            return self.my_socket.recv(512)

    def readAll(self):
        self.my_socket.send("SP6T:ALL:STATE?\n")
        return self.my_socket.recv(512)


    def setOne(self, numberOfSwitch, numberOfPort):
        if numberOfSwitch > 6 or numberOfSwitch < 0:
            return "Value " + str(numberOfSwitch) + " isn't correct!"
        elif numberOfPort > 6 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.my_socket.send("SP6T:" + str(numberOfSwitch) + ":STATE:" + str(numberOfPort) + "\n")
            return self.my_socket.recv(512)


    def setAll(self, numberOfPort):
        if numberOfPort > 6 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.my_socket.send("SP6T:ALL:STATE:" + str(numberOfPort)*6 + "\r\n")
            return self.my_socket.recv(512)

    def clearAll(self):
        return self.setAll(0)

class ATTjfw(object):  #port 3001

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.telnet = telnetlib.Telnet(self.ip, self.port, 1)
        self.telnet.read_until('No MOTD has been set\r\n\n')

    def readOne(self, numberOfPort):
        if numberOfPort > 16 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.telnet.write("ra" + str(numberOfPort) + "\r\n")
            return self.telnet.read_until("\r\n\n").replace("\r", "")

    def readAll(self):
        self.telnet.write("raa\r\n")
        return self.telnet.read_until("\r\n\n").replace("\r", "")


    def setOne(self, numberOfPort, dB):
        if numberOfPort > 16 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        elif dB < 0 or dB > 127:
            return "Value " + str(dB) + " isn't correct!"
        else:
            self.telnet.write("sar" + str(numberOfPort) + " " + str(dB) + "\r\n")
            return self.telnet.read_until("\r\n\n").replace("\r", "")

    def setAll(self, dB):
        if dB < 0 or dB > 127:
            return "Value " + str(dB) + " isn't correct!"
        else:
            self.telnet.write("saa " + str(dB) + "\r\n")
            return self.telnet.read_until("\r\n\n").replace("\r", "")

    def clearAll(self):
        return self.setAll(127)

class MS252jfw(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.telnet = telnetlib.Telnet(self.ip, self.port, 1)
        self.telnet.read_until('JFW>> ')


    def readOne(self, numberOfPort):
        if numberOfPort > 8 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.telnet.write("RO" + str(numberOfPort) + "\r\n")
            return self.telnet.read_until('\r\n\n\rJFW>> ').replace('\r\n\n\rJFW>> ', '\n').replace("\n\rJFW>> ","")


    def readAll(self):
        for i in range(1, 9):
            self.telnet.write("RO" + str(i) + "\r\n")
        self.telnet.write("!\r\n")
        return self.telnet.read_until('Command not found: !\r\n\n\rJFW>> ').replace('Command not found: !\r\n',
                                                                                    '').replace("\n\rJFW>> ",
                                                                                                "").replace("\r", "")

    def setOne(self, numberOfPort1, numberOfPort2):
        if numberOfPort1 > 8 or numberOfPort1 < 0:
            return "Value " + str(numberOfPort1) + " isn't correct!"
        elif numberOfPort2 > 8 or numberOfPort2 < 0:
            return "Value " + str(numberOfPort2) + " isn't correct!"
        else:
            self.telnet.write("SO" + str(numberOfPort1) + " " + str(numberOfPort2) + "\r\n")
            return self.readOne(numberOfPort1)


    def setAll(self, numberOfPort):
        if numberOfPort > 8 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            snumberOfPort = " " + str(numberOfPort) + "\r\n"
            for i in range(1, 9):
                self.telnet.write("SO" + str(i) + snumberOfPort)
        return self.readAll()

    def clearAll(self):
        return self.setAll(0)

class MS299jfw(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.telnet = telnetlib.Telnet(self.ip, self.port, 1)
        self.telnet.read_until('JFW>> ')


    def readOne(self, numberOfPort):
        if numberOfPort > 4 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            self.telnet.write("RO" + str(numberOfPort) + "\r\n")
            return self.telnet.read_until('\r\n\n\rJFW>> ').replace('\r\n\n\rJFW>> ', '\n').replace("\n\rJFW>> ", "")

    def readAll(self):
        for i in range(1, 5):
            self.telnet.write("RO" + str(i) + "\r\n")
        self.telnet.write("!\r\n")
        return self.telnet.read_until('Command not found: !\r\n\n\rJFW>> ').replace('Command not found: !\r\n',
                                                                                    '').replace("\n\rJFW>> ",
                                                                                                "").replace("\r", "")

    def setOne(self, numberOfPort1, numberOfPort2):
        if numberOfPort1 > 4 or numberOfPort1 < 0:
            return "Value " + str(numberOfPort1) + " isn't correct!"
        elif numberOfPort2 > 8 or numberOfPort2 < 0:
            return "Value " + str(numberOfPort2) + " isn't correct!"
        else:
            self.telnet.write("SO" + str(numberOfPort1) + " " + str(numberOfPort2) + "\r\n")
            return self.readOne(numberOfPort1)


    def setAll(self, numberOfPort):
        if numberOfPort > 8 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            snumberOfPort = " " + str(numberOfPort) + "\r\n"
            for i in range(1, 5):
                self.telnet.write("SO" + str(i) + snumberOfPort)
        return self.readAll()

    def clearAll(self):
        return self.setAll(0)


class MSeubus(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.telnet = telnetlib.Telnet(ip, port, 1)
        self.telnet.read_until("commands.\r\n\r\n")

    def readAll(self):
        self.telnet.write("con ?\r\n")
        self.telnet.write("!\r\n")
        a = self.telnet.read_until("not found\r\n")
        return a.replace("# con ?\r\n", "").replace("# !\r\n!: not found\r\n", "").replace("\r", "").replace("!\n", "")

    def readOne(self, numberOfPort):
        if numberOfPort > 8 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            a = self.readAll()
            list = a.split("\n")
            return list[numberOfPort - 1]

    def setOne(self, numberOfPort1, numberOfPort2):
        if numberOfPort1 > 8 or numberOfPort1 < 0:
            return "Value " + str(numberOfPort1) + " isn't correct!"
        elif numberOfPort2 > 8 or numberOfPort2 < 0:
            return "Value " + str(numberOfPort2) + " isn't correct!"
        else:
            self.telnet.write("con " + str(numberOfPort1) + " " + str(numberOfPort2) + "\r\n")
            if self.telnet.read_until("ok\r\n") == "# con " + str(numberOfPort1) + " " + str(numberOfPort2) + "\r\nok\r\n":
                return "  " + str(numberOfPort1) + "  <--->  " + str(numberOfPort2) + " done\n"
            else:
                return ":("

    def setAll(self, numberOfPort2):
        return "You can't set the same \nvalue on all ports!"

    def clearAll(self):
        list = []
        list.append(self.setOne(1, 0))
        list.append(self.setOne(2, 0))
        list.append(self.setOne(3, 0))
        list.append(self.setOne(4, 0))
        list.append(self.setOne(5, 0))
        list.append(self.setOne(6, 0))
        list.append(self.setOne(7, 0))
        list.append(self.setOne(8, 0))
        return "".join(list)

class ATThbte(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.telnet = telnetlib.Telnet(ip, port, 1)
        self.telnet.write("user 1\r\n")
        self.telnet.write("!\r\n")
        self.telnet.read_until('FAIL\r\nRadio Rack>')

    def readOne(self, numberOfPort):
        if numberOfPort > 24 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        else:
            list = self.readAll().split("\n")
            result = ""
            for att in list:
                if " " + str(numberOfPort) + " " in att:
                    result = att
            return result

    def readAll(self):
        self.telnet.write("att\r\n")
        self.telnet.write("!\r\n")
        return self.telnet.read_until('FAIL\r\nRadio Rack>').replace("\r\nRadio Rack>FAIL\r\nRadio Rack>", "").replace(";", "\n")

    def setOne(self, numberOfPort, dB):
        if numberOfPort > 24 or numberOfPort < 0:
            return "Value " + str(numberOfPort) + " isn't correct!"
        elif dB < 0 or dB > 127:
            return "Value " + str(dB) + " isn't correct!"
        else:
            self.telnet.write("att " + str(numberOfPort) + " " + str(dB) + "\r\n")
            return self.telnet.read_until("Radio Rack>").replace("\r\nRadio Rack>", "")

    def setAll(self, dB):
        if dB < 0 or dB > 127:
            return "Value " + str(dB) + " isn't correct!"
        else:
            list = []
            endOfStr = " " + str(dB) + ";"
            for numberOfPort in range(1, 25):
                list.append("att " + str(numberOfPort) + endOfStr)
            comp = "".join(list)
            comp = comp[:-1] + "\r\n"
            self.telnet.write(comp)
            self.telnet.write("!\r\n")
            return self.telnet.read_until('FAIL\r\nRadio Rack>').replace("\r\nRadio Rack>FAIL\r\nRadio Rack>", "").replace(";", "\n")

    def clearAll(self):
        return self.setAll(127)


if __name__ == "__main__":
    HOST1 = "10.42.170.39"  # att 50pa-737  -- 'TCP/IP User Block Console Version 1.1\r\n'   [3001, ATTjfw]
    HOST2 = "10.42.170.92"  # att hbte 24-24  -- "Welcome to use 'Programmable Attenuator 24-24' of HBTE\r\nUserName:"[23, ATThbte]nie dziala
    HOST3 = "10.42.170.86"  # ms 50-ms-252/ms 50-ms-299  -- 'TCP/IP User Block Console Version 1.1\r\n'[3001, MSjfw]
    HOST5 = "10.42.170.87"  # ms eubus  -- '' [23, MSeubus] wolno dziala telnet//nie dziala
    HOST6 = "10.42.170.49"  # rfs ztm15 --  '\r\n' [23, RFSztm15]
    HOST7 = "10.42.170.50"  # rfs zte196 --  ''  [23, RFSzte196]

    at1 = "10.42.170.6"    #'50PA-737 Connection Open\r\nNo MOTD has been set\r\n\n'
    at2 = "10.42.170.39"   #'50PA-684 Connection Open\r\nNo MOTD has been set\r\n\n'
    at3 = "10.42.170.8"    #'50PA-765 Connection Open\r\nNo MOTD has been set\r\n\n'
    at4 = "10.42.170.38"   #'50PA-737 Connection Open\r\nNo MOTD has been set\r\n\n'
    at5 = "10.42.170.84"   #'50PA-557 Connection Open\r\nNo MOTD has been set\r\n\n'

    ms1 = "10.42.170.85"   #'50MS-252 Connection Open\r\nNo MOTD has been set\r\n\n\rJFW>> '
    ms2 = "10.42.170.86"   #'50MS-252 Connection Open\r\nNo MOTD has been set\r\n\n\rJFW>> '
    ms3 = "10.42.170.89"   #'50MS-299 Connection Open\r\nNo MOTD has been set\r\n\n\rJFW>> '
    ms4 = "10.42.170.88"   #'50MS-299 Connection Open\r\nNo MOTD has been set\r\n\n\rJFW>> '




