# -*- coding: utf-8 -*-
"""
:copyright: Nokia Networks
:author: volha_hatskevich
:contact: volha.hatskevich@nokia.com
:maintainer: None
:contact: None
"""

from testowa.telnet import TelnetConnection
import datetime


class RFSzte196(TelnetConnection):
    """Class that control telnet connection to RFS ZTE196"""

    def _read_one_telnet_command(self, number_of_switch):
        """
        Read number of port on desired switch section
        :param number_of_switch: number of switch section
        :return: formatting string that show number of port on desired switch number
        """
        number_of_port = self.read_from_telnet(("GetSSW{}\n\r".format(str(number_of_switch))))
        return 'Sw {} = {}\n'.format(str(number_of_switch), number_of_port)

    def _set_one_telnet_command(self, number_of_switch, number_of_port):
        """
        Set desired number of port on switch section
        :param number_of_switch: number of switch section
        :param number_of_port: number of port
        :return: answer of telnet
        """
        result = self.read_from_telnet(("c{}={}\n\r".format(str(number_of_switch), str(number_of_port))))
        return result

    def read_one(self, number_of_switch):
        """
        Get number of port on switch section
        :param number_of_switch: number of switch section
        :return: formatting string with number of switch section and it's port number
        """
        if number_of_switch > 12 or number_of_switch < 0:
            return "Value {} isn't correct!".format(number_of_switch)
        else:
            return self._read_one_telnet_command(number_of_switch)

    def read_all(self):
        """Get number of ports are mapped with switch sections
        :return: formatting string with numbers of switch section and it's port number
        """
        result = ""
        for i in range(1, 13):
            result += self._read_one_telnet_command(i)
        return result

    def set_one(self, number_of_switch, number_of_port):
        """

        :param number_of_switch:
        :param number_of_port:
        :return: telnet reply
        """
        if number_of_switch > 12 or number_of_switch < 0:
            return "Value {} isn't correct!".format(number_of_switch)
        elif number_of_port > 6 or number_of_port < 0:
            return "Value {} isn't correct!".format(number_of_port)
        else:
            return self._set_one_telnet_command(number_of_switch, number_of_port)

    def set_all(self, number_of_port):
        """
        Set all switch sections on the same number of port
        :param number_of_port: number of port
        :return: telnet reply
        """
        if number_of_port > 6 or number_of_port < 0:
            return "Value {} isn't correct!".format(number_of_port)
        else:
            result = ""
            for i in range(1, 13):
                result += self._set_one_telnet_command(i, number_of_port)
            return result

    def clear_all(self):
        """
        Set all switch sections on 0
        :return: telnet reply
        """
        return self.set_all(0)

if __name__ == "__main__":
    start_time = datetime.datetime.now()
    a = RFSzte196()
    a.connect("10.42.170.50", 23)
    print(a.set_one(1, 2))
    print("\n")
    print(a.read_one(1))
    print("\n")
    print(a.set_all(3))
    print("\n")
    print(a.read_all())
    print("\n")
    print(a.clear_all())
    print("\n")
    print(a.read_all())
    a.disconnect()
    finish_time = datetime.datetime.now()
    print(finish_time - start_time)
