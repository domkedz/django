from django.conf.urls import url, include
from django.contrib import admin

from testowa import views

urlpatterns = [
    url(r'^$', views.index, name='Index'),
    url(r'^output/$', views.output, name='out')
]
