# -*- coding: utf-8 -*-
"""
:copyright: Nokia Networks
:author: volha_hatskevich
:contact: volha.hatskevich@nokia.com
:maintainer: None
:contact: None
"""

from devices_lib.telnet import TelnetConnection
import datetime


class RFSztm15(TelnetConnection):
    """Class that control telnet connection to RFS ZTM15"""

    def read_one(self, number_of_switch):
        """
        Get number of port on switch section
        :param number_of_switch: number of switch section
        :return: formatting string with number of switch section and it's port number
        """
        if number_of_switch > 6 or number_of_switch < 0:
            return "Value {} isn't correct!".format(number_of_switch)
        else:
            number_of_port = self.read_from_telnet(("SP6T:{}:STATE?\n\r".format(str(number_of_switch))))
            return 'Sw {} = {}\n'.format(str(number_of_switch), number_of_port)

    def read_all(self):
        """
        Get number of ports mapped with switch sections
        :return: formatting string with numbers of switch section and it's port number
        """
        list_of_ports = list(self.read_from_telnet("SP6T:ALL:STATE?\n\r"))
        for number, elem in enumerate(list_of_ports):
            list_of_ports[number] = 'Sw {} = {}\n'.format(str(number+1), elem)
        return ''.join(list_of_ports)

    def set_one(self, number_of_switch, number_of_port):
        """
        Set switch section on desired number of port
        :param number_of_switch:
        :param number_of_port:
        :return: telnet reply
        """
        if number_of_switch > 6 or number_of_switch < 0:
            return "Value {} isn't correct!".format(number_of_switch)
        elif number_of_port > 6 or number_of_port < 0:
            return "Value {} isn't correct!".format(number_of_port)
        else:
            number_of_port = self.read_from_telnet(("SP6T:{}:STATE:{}\n\r".format(str(number_of_switch),str(number_of_port))))
            return number_of_port

    def set_all(self, number_of_port):
        """
        Set all switch sections on the same number of port
        :param number_of_port: number of port
        :return: telnet reply
        """
        if number_of_port > 6 or number_of_port < 0:
            return "Value {} isn't correct!".format(number_of_port)
        else:
            number_of_port = self.read_from_telnet(
                ("SP6T:ALL:STATE:{}\n\r".format(str(number_of_port)*6)))
            return number_of_port

    def clear_all(self):
        """
        Set all switch sections on 0
        :return: telnet reply
        """
        return self.set_all(0)


if __name__ == "__main__":
    start_time = datetime.datetime.now()
    a = RFSztm15()
    a.connect("10.42.170.49", 23)
    print(a.set_one(1, 2))
    print("\n")
    print(a.read_one(1))
    print("\n")
    print(a.set_all(3))
    print("\n")
    print(a.read_all())
    print("\n")
    print(a.clear_all())
    print("\n")
    print(a.read_all())
    a.disconnect()
    finish_time = datetime.datetime.now()
    print(finish_time - start_time)

