# -*- coding: utf-8 -*-
"""
:copyright: Nokia Networks
:author: volha_hatskevich
:contact: volha.hatskevich@nokia.com
:maintainer: None
:contact: None
"""

import telnetlib
import socket
import time
import datetime

class TelnetConnection(object):
    """Class that control telnet connection to devices"""
    def __init__(self):
        self.telnet = None

    def connect(self, ip, port):
        """
        Establish connection by Telnet
        :param ip: device connection ip
        :param port: device connection port
        """
        try:
            self.telnet = telnetlib.Telnet(ip, port, 5)
            time.sleep(0.05)
            self.telnet.read_very_eager()
        except (TimeoutError, socket.timeout):
            print("Connection error")

    def disconnect(self):
        """Close telnet session for current device"""
        try:
            self.telnet.close()
        except AttributeError:
            print("Disconnection error")

    def read_from_telnet(self, command):
        """
        nieskonczona metoda co jesli polaczenie zostanie utracone
        Write telnet command and get telnet reply.
        :param command: command that do something with device(set port, get port...)
        :return: telnet reply
        """
        self.telnet.write(command.encode('ascii'))
        time.sleep(0.05)
        return (str(self.telnet.read_very_eager())).replace('\\n\\r', '').replace('b', '').replace('\'', '')

    def read_one(self, number):
        pass

    def read_all(self):
        pass

    def set_one(self, number, value):
        pass

    def set_all(self, value):
        pass

    def clear_all(self):
        pass


